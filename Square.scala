package queens

/**
  * Created by sergo on 10.3.17.
  *
  * There are three types of Square:
  * SqSafe   - not under attack
  * SqUnsafe - under Queen's attack
  * SqQueen  - with placed Queen
  */

trait Square

case class SqSafe() extends Square
object SqSafe extends Square

case class SqUnsafe() extends Square
object SqUnsafe extends Square

case class SqQueen() extends Square
object SqQueen extends Square
