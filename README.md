# n queens problem #

This program is written in Scala and it finds all possible solutions of "n queens problem" for any defined board's size.

A bit more info read here: https://en.wikipedia.org/wiki/Eight_queens_puzzle

### What is this repository for? ###

It's an example of algorithm finding all possible solutions for "n queens problem", written in a pure functional style (except printing of result), without any mutable variables.

### Requirements ###

* Scala (2.12)

### How to run application ###

```bash
$> cd path/to/application
$> scalac *.scala
$> scala queens.QueensApp
```

### Resources ###

* output_8x8.txt - result of printing all of 92 solutions for "Eight queens puzzle"
