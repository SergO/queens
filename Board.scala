package queens

/**
  * Created by sergo on 11.3.17.
  *
  * @constructor creates a new empty Board if only first parameter `n` is given
  * @param n board's size (n x n), used only to create new empty board
  * @param squares contains all squares of any defined subtype of Square
  * @param queens contains coordinates of all placed queens
  *
  */
class Board(val n: Int, squares: List[List[Square]], val queens: List[Coord]) {
  def this(n: Int) = this(n, List.fill(n)(List.fill(n)(SqSafe)), List())

  /*
  * Returns a new Board with new Queen and marked squares under her attack as Unsafe (except squares occupied by others Queens)
  * */
  def placeQueen(queenCoord: Coord): Board = {

    def updSquares(squares: List[List[Square]], rowNum: Int): List[List[Square]] = {

      def updRow(row: List[Square], colNum: Int): List[Square] = {

        def updSquare(square: Square): Square = {
          val queenRowIdx = queenCoord.rowIdx
          val queenCol = queenCoord.colIdx

          if (queenCoord.rowIdx == rowNum && queenCol == colNum) SqQueen
          else if ( queenCoord.rowIdx == rowNum || queenCol == colNum ||
                    Math.abs(queenCoord.rowIdx - rowNum) == Math.abs(queenCol - colNum) ) square match {
            case SqQueen => SqQueen
            case _ => SqUnsafe
          }
          else square

        }

        row match {
          case List() => row
          case square :: squaresTail => updSquare(square) :: updRow(squaresTail, colNum + 1)
        }
      }

      squares match {
        case List() => squares
        case row :: rowsTail => updRow(row, 0) :: updSquares(rowsTail, rowNum + 1)
      }
    }

    new Board(n, updSquares(this.squares, 0), queenCoord :: this.queens)
  }

  def isSquareSafe(squareCoord: Coord): Boolean =
    squares(squareCoord.rowIdx)(squareCoord.colIdx) match {
      case SqSafe => true
      case _ => false
    }

  /*
   * Prints board
   * */
  def show(): Unit = {
    def prnSquare(square: Square) = square match {
      case SqSafe => print("| . ")
      case SqUnsafe => print("| x ")
      case SqQueen => print("| Q ")
    }

    def prnRow(row: List[Square]): List[Square] = row match {
      case List() =>
        println("|")
        Nil
      case square :: squaresTail =>
        prnSquare(square)
        prnRow(squaresTail)
    }

    def prnBoard(squares: List[List[Square]]): List[List[Square]] = squares match {
      case List() =>
        println()
        Nil
      case row :: rowsTail =>
        prnRow(row)
        prnBoard(rowsTail)
    }

    println()
    prnBoard(this.squares)
  }
}
