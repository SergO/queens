package queens

/**
  * Created by sergo on 10.3.17.
  */
object QueensApp {
  def main(args: Array[String]): Unit = {

    // MAIN PARAMETER: BOARD'S SIZE (e.g. n = 8 is for classic chess board 8x8)
    val n = 8

    /*
    * Main recursive function
    * looks through all possible combinations
    *
    * Uncomment lines for visual tracking of process (recommended for small values of `n`)
    * */
    def boardCollector(currentCoordinates: Coord, currentBoard: Board, successBoards: List[Board]): List[Board] = {
//      println(currentCoordinates)

      val isSquareSafe = currentBoard.isSquareSafe(currentCoordinates)
      val isLastQueenNeeded = currentBoard.queens.length + 1 == n

      if (isSquareSafe && isLastQueenNeeded) {
        val solvedBoard = currentBoard.placeQueen(currentCoordinates)

//        println("GOAL!!!")
//        currentBoard.show()
//        solvedBoard.show()

        solvedBoard :: successBoards
      }

      else if (currentCoordinates.rowIdx + 1 == n && currentCoordinates.colIdx + 1 == n) {

//        println("The Last Square")
//        currentBoard.show()

        successBoards
      }

      else {
        val nextCoordinates =
          if (currentCoordinates.colIdx + 1 == n)
            Coord(currentCoordinates.rowIdx + 1, 0)
          else
            Coord(currentCoordinates.rowIdx, currentCoordinates.colIdx + 1)

        if (isSquareSafe) {
          val oneQueenMoreBoard = currentBoard.placeQueen(currentCoordinates)

//          println("A Safe Square")
//          currentBoard.show()
//          oneQueenMoreBoard.show()

          boardCollector(nextCoordinates, currentBoard, successBoards) :::
          boardCollector(nextCoordinates, oneQueenMoreBoard, successBoards)
        }
        else {

//          println("An Unsafe Square")
//          currentBoard.show()

          boardCollector(nextCoordinates, currentBoard, successBoards)
        }
      }

    }

    val successBoards = boardCollector(Coord(0, 0), new Board(n), List())
    println("Number of found solutions", successBoards.length)
    for (a <- successBoards) a.show()

  }
}
